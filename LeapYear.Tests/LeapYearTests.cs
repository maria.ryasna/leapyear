﻿using NUnit.Framework;

namespace LeapYear
{
    [TestFixture]
    public class LeapYearServiceTests
    {
        [TestCase(1996)]
        [TestCase(2004)]
        [TestCase(2008)]
        public void IsLeapYear_YearDivisibleBy4_ReturnTrue(int year)
        {
            // Arrange
            LeapYearService service = new LeapYearService();

            // Act
            bool actual = service.IsLeapYear(year);

            // Assert
            Assert.True(actual);
        }

        [TestCase(1991)]
        [TestCase(2001)]
        [TestCase(2009)]
        public void IsLeapYear_YearNotDivisibleBy4_ReturnFalse(int year)
        {
            // Arrange
            LeapYearService service = new LeapYearService();

            // Act
            bool actual = service.IsLeapYear(year);

            // Assert
            Assert.False(actual);
        }

        [TestCase(1700)]
        [TestCase(1900)]
        [TestCase(2200)]
        public void IsLeapYear_YearDivisibleBy100ButNotBy400_ReturnFalse(int year)
        {
            // Arrange
            LeapYearService service = new LeapYearService();

            // Act
            bool actual = service.IsLeapYear(year);

            // Assert
            Assert.False(actual);
        }

        [TestCase(2000)]
        [TestCase(2400)]
        [TestCase(2800)]
        public void IsLeapYear_YearDivisibleBy100AndBy400_ReturnTrue(int year)
        {
            // Arrange
            LeapYearService service = new LeapYearService();

            // Act
            bool actual = service.IsLeapYear(year);

            // Assert
            Assert.True(actual);
        }
    }
}
