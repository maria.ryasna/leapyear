﻿using System;

namespace LeapYear.Core
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter year:");
            string year = Console.ReadLine();

            try
            {
                LeapYearService service = new LeapYearService();
                bool isLeapYear = service.IsLeapYear(int.Parse(year));
                Console.WriteLine($"Year {year} is leap: {isLeapYear}");
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine($"[ERROR]: {ex.Message}");
            }
            
            Console.ReadKey();
        }
    }
}
