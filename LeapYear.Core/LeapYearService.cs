﻿using System;

namespace LeapYear
{
    public class LeapYearService
    {
        public LeapYearService()
        {
        }

        public bool IsLeapYear(int year)
        {
            if (year < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(year));
            }
            if ((year % 100 == 0 && year % 400 != 0))
            {
                return false;
            }
            return year % 4 == 0;
        }
    }
}
